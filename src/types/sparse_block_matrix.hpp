namespace srrg_g2o_slim {

//ia helpers
template <class MatrixType>
inline bool _scalarProdStructure(const typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap& row1_,
                                 const typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap& row2_,
                                 const int max_pos_) {
  typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap::const_iterator it_1 = row1_.begin();
  typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap::const_iterator it_2 = row2_.begin();
  while(it_1 != row1_.end() && it_2 != row2_.end()){
    int col_idx_1 = it_1->first;
    int col_idx_2 = it_2->first;
    if(col_idx_1 > max_pos_ || col_idx_2 > max_pos_)
      return false;
    if(col_idx_1 == col_idx_2)
      return true;
    else if(col_idx_1 > col_idx_2)
      ++it_2;
    else if(col_idx_1 < col_idx_2)
      ++it_1;
  }
  return false;
}

template <class MatrixType>
inline MatrixType _scalarProd(const typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap& row1_,
                              const typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap& row2_,
                              const int max_pos_) {

  typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap::const_iterator it_1 = row1_.begin();
  typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap::const_iterator it_2 = row2_.begin();
  MatrixType result = MatrixType::Zero();
  MatrixType value = MatrixType::Zero();
  while(it_1 != row1_.end() && it_2 != row2_.end()){
    const int col_idx_1 = it_1->first;
    const int col_idx_2 = it_2->first;
    if(col_idx_1 > max_pos_ || col_idx_2 > max_pos_){
      return result;
    }
    if(col_idx_1 == col_idx_2){
      result.noalias() += (*it_1->second) * it_2->second->transpose();
      ++it_1;
      ++it_2;
    }
    else if(col_idx_1 > col_idx_2)
      ++it_2;
    else if(col_idx_1 < col_idx_2)
      ++it_1;
  }
  return result;
}



template <class MatrixBlock>
SparseBlockMatrix<MatrixBlock>::SparseBlockMatrix(const SparseBlockMatrix<MatrixBlock>& other_,
                                                  const bool deep_copy_flag_) {
  _num_rows = other_._num_rows;
  _num_cols = other_._num_cols;
  _nnz = 0;
  _has_storage = deep_copy_flag_;

  _rows_vector.resize(_num_rows);
  for (int row_index = 0; row_index < _num_rows; ++row_index) {
    const IntMatrixBlockPtrMap& other_row = other_._rows_vector[row_index];

    typename IntMatrixBlockPtrMap::const_iterator other_block_it = other_row.begin();
    typename IntMatrixBlockPtrMap::const_iterator other_row_end = other_row.end();
    while (other_block_it != other_row_end) {
      const int& col_index = other_block_it->first;

      //ia copy only if needed
      if (_has_storage) {
        MatrixBlock* new_block = _block_manager.allocateOneBlock(row_index, col_index);
        *new_block = (*other_block_it->second);
        setBlockPtr(row_index, col_index, new_block);
      } else {
        setBlockPtr(row_index, col_index, other_block_it->second);
      }

      ++other_block_it;
    }
  }

  if (_nnz != other_._nnz) {
    throw std::runtime_error("dimension mismatch between the two matrices, exit");
  }

  _is_initialized = true;
}

template <class MatrixBlock>
SparseBlockMatrix<MatrixBlock>::SparseBlockMatrix(const int num_rows_,
                                                  const int num_cols_,
                                                  const MatrixBlockManager<MatrixBlock>& block_manager_) {
  _has_storage = false;
  _num_rows = num_rows_;
  _num_cols = num_cols_;
  _rows_vector.resize(_num_rows);

  _nnz = 0;

  const typename MatrixBlockManager<MatrixBlock>::IntPairBlockPtrMap& blocks_pull = block_manager_.getBlocksMap();

  typename MatrixBlockManager<MatrixBlock>::IntPairBlockPtrMap::const_iterator block_it = blocks_pull.begin();
  typename MatrixBlockManager<MatrixBlock>::IntPairBlockPtrMap::const_iterator blocks_pull_end = blocks_pull.end();
  while (block_it != blocks_pull_end) {
    setBlockPtr(block_it->first.first, block_it->first.second, block_it->second);
    ++block_it;
  }

  if (_nnz != block_manager_.dimension()) {
    throw std::runtime_error("dimension mismatch, exit");
  }

  _is_initialized = true;

}

template <class MatrixBlock>
SparseBlockMatrix<MatrixBlock>::SparseBlockMatrix(const int num_rows_,
                                                  const int num_cols_) {
  _has_storage = true;
  _num_rows = num_rows_;
  _num_cols = num_cols_;
  _rows_vector.resize(_num_rows);

  _nnz = 0;

  _is_initialized = true;

}

template <class MatrixBlock>
SparseBlockMatrix<MatrixBlock>::~SparseBlockMatrix() {
  if (_has_storage) {
    _block_manager.reset();
  }

  for (int row_index = 0; row_index < _num_rows; ++row_index) {
    _rows_vector[row_index].clear();
  }

  _rows_vector.clear();
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::reset() {
  if (_has_storage) {
    _block_manager.reset();
  }

  for (int row_index = 0; row_index < _num_rows; ++row_index) {
    _rows_vector[row_index].clear();
  }

  _rows_vector.clear();
  _nnz = 0;
  _num_rows = 0;
  _num_cols = 0;
  _has_storage = false;
  _is_initialized = false;
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::setZero() {
  for (int row_index = 0; row_index < _num_rows; ++row_index) {
    IntMatrixBlockPtrMap& row = _rows_vector[row_index];
    typename IntMatrixBlockPtrMap::iterator block_it = row.begin();
    typename IntMatrixBlockPtrMap::iterator row_end = row.end();
    while (block_it != row_end) {
      block_it->second->setZero();
      ++block_it;
    }
  }
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::setRandom() {
  for (int row_index = 0; row_index < _num_rows; ++row_index) {
    IntMatrixBlockPtrMap& row = _rows_vector[row_index];
    typename IntMatrixBlockPtrMap::iterator block_it = row.begin();
    typename IntMatrixBlockPtrMap::iterator row_end = row.end();
    while (block_it != row_end) {
      block_it->second->setRandom();
      ++block_it;
    }
  }
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::setIdentity() {
  for (int row_index = 0; row_index < _num_rows; ++row_index) {
    IntMatrixBlockPtrMap& row = _rows_vector[row_index];
    typename IntMatrixBlockPtrMap::iterator block_it = row.begin();
    typename IntMatrixBlockPtrMap::iterator row_end = row.end();
    while (block_it != row_end) {
      block_it->second->setIdentity();
      ++block_it;
    }
  }
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::savePattern(const std::string& filename_) const {
  std::ofstream file(filename_);
  for (int row_index = 0; row_index < _num_rows; ++row_index) {
    for (int col_index = 0; col_index < _num_cols; ++col_index) {
      if (isNonZero(row_index, col_index)) {
        file << 1 << " ";
      } else {
        file << 0 << " ";
      }
    }
    file << std::endl;
  }
  file << std::endl;
  file.close();
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::load(const std::string& filename_) {

  std::fstream file(filename_);
  std::string line;

  int readed_num_rows = -1;
  int readed_num_cols = -1;
  getline(file, line);
  std::stringstream first_ss(line);
  first_ss >> readed_num_rows >> readed_num_cols;

  if (readed_num_rows != _num_rows ||
      readed_num_cols != _num_cols) {
    throw std::runtime_error("dimension mismatch");
  }

  while (getline(file, line)) {
    int block_row = -1;
    int block_col = -1;
    int block_dim_r = -1;
    int block_dim_c = -1;
    float value = 0;

    std::stringstream ss(line);
    ss >> block_row >> block_col;
    ss >> block_dim_r >> block_dim_c;
    MatrixBlock block = MatrixBlock::Zero();
    for (int r = 0; r < block_dim_r; ++r) {
      for (int c = 0; c < block_dim_c; ++c) {
        ss >> block(r,c);
      }
    }

    if (!block.isZero()) {
      MatrixBlock* new_block = createBlock(block_row, block_col);
      *new_block = block;
    }
  }
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::print() const {
  std::cerr << "print SparseBlockMatrix" << std::endl;
  for (int row_index = 0; row_index < _num_rows; ++row_index) {
    const IntMatrixBlockPtrMap& row = _rows_vector[row_index];
    typename IntMatrixBlockPtrMap::const_iterator block_it = row.begin();
    typename IntMatrixBlockPtrMap::const_iterator row_end = row.end();
    while (block_it != row_end) {
      const int& col_index = block_it->first;
      printBlock(row_index, col_index);
      ++block_it;
    }
  }
  std::cerr << std::endl;
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::printBlock(const int row_,
                                                const int col_) const {
  if (row_ < 0 || row_ >= _num_rows ||
      col_ < 0 || col_ >= _num_cols) {
    throw std::runtime_error("index out of bound");
  }

  const MatrixBlock* block = getBlockPtr(row_, col_);
  if (!block) {
    std::cerr << "warning, non initialized block" << std::endl;
  } else {
    std::cerr << "Block(" << row_ << "," << col_ << ")" << std::endl;
    std::cerr << *block << std::endl;
  }

}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::setBlockPtr(const int row_,
                                                 const int col_,
                                                 MatrixBlock* block_ptr_) {
  if (row_ < 0 || row_ >= _num_rows ||
      col_ < 0 || col_ >= _num_cols) {
    throw std::runtime_error("index out of bound");
  }

  IntMatrixBlockPtrMap& row = _rows_vector[row_];
  typename IntMatrixBlockPtrMap::iterator block_it = row.find(col_);

  //ia if block does not exist and block_ptr is a non zero block we place it in the view
  //ia if a block already exists in <row_, col_>, we overwrite the pointer or
  //ia we remove it from the view depending if the new pointer is non_zero or not
  if (block_it == row.end()) {
//    if (!block_ptr_->isZero()) {
      row.insert(std::make_pair(col_,block_ptr_));
      ++_nnz;
//    }
  } else {
    block_it->second = block_ptr_;
  }

}

template <class MatrixBlock>
MatrixBlock* SparseBlockMatrix<MatrixBlock>::getBlockPtr(const int row_,
                                                         const int col_) const {
  if (row_ < 0 || row_ >= _num_rows ||
      col_ < 0 || col_ >= _num_cols) {
    throw std::runtime_error("index out of bound");
  }

  const IntMatrixBlockPtrMap& row = _rows_vector[row_];
  typename IntMatrixBlockPtrMap::const_iterator block_it = row.find(col_);

  if (block_it == row.end()) {
    return nullptr;
  }

  return block_it->second;
}

template <class MatrixBlock>
MatrixBlock* SparseBlockMatrix<MatrixBlock>::createBlock(const int row_,
                                                         const int col_) {
  if (!_has_storage) {
    throw std::runtime_error("matrix does not own the blocks");
  }

  if (row_ < 0 || row_ >= _num_rows ||
      col_ < 0 || col_ >= _num_cols) {
    throw std::runtime_error("index out of bound");
  }

  IntMatrixBlockPtrMap& row = _rows_vector[row_];
  typename IntMatrixBlockPtrMap::iterator block_it = row.find(col_);

  //ia if there is already a block in <row_, col_>
  if (row.count(col_)) {
    throw std::runtime_error("block already existing");
  }

  MatrixBlock* new_block = _block_manager.allocateOneBlock(row_, col_);
  setBlockPtr(row_, col_, new_block);
  return new_block;
}

template <class MatrixBlock>
const MatrixBlock& SparseBlockMatrix<MatrixBlock>::at(const int row_,
                                                      const int col_) const {
  if (row_ < 0 || row_ >= _num_rows ||
      col_ < 0 || col_ >= _num_cols) {
    throw std::runtime_error("index out of bound");
  }

  const IntMatrixBlockPtrMap& row = _rows_vector[row_];
  typename IntMatrixBlockPtrMap::const_iterator block_it = row.find(col_);
  if (block_it == row.end()) {
    throw std::runtime_error("block does not exist");
  }
  return *block_it->second;
}

template <class MatrixBlock>
MatrixBlock& SparseBlockMatrix<MatrixBlock>::at(const int row_,
                                                const int col_) {
  if (row_ < 0 || row_ >= _num_rows ||
      col_ < 0 || col_ >= _num_cols) {
    throw std::runtime_error("index out of bound");
  }

  IntMatrixBlockPtrMap& row = _rows_vector[row_];
  typename IntMatrixBlockPtrMap::iterator block_it = row.find(col_);
  if (block_it == row.end()) {
    throw std::runtime_error("block does not exist");
  }
  return *block_it->second;
}

template <class MatrixBlock>
const bool SparseBlockMatrix<MatrixBlock>::isNonZero(const int row_,
                                                     const int col_) const {
  const MatrixBlock* block = getBlockPtr(row_, col_);
  if (block && !block->isZero()) {
    return true;
  }
  return false;
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::removeRow(const int row_idx_) {
  if (row_idx_ < 0 || row_idx_ >= _num_rows) {
    throw std::runtime_error("row index out of bound");
  }

  const int num_removed_blocks = _rows_vector[row_idx_].size();

  typename IntMatrixBlockPtrMapVector::iterator row_iterator = _rows_vector.begin() + row_idx_;
  _rows_vector.erase(row_iterator);

  --_num_rows;
  _nnz -= num_removed_blocks;

}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::removeCol(const int col_idx_) {
  if (col_idx_ < 0 || col_idx_ >= _num_cols) {
    throw std::runtime_error("col index out of bound");
  }

  int new_col_index = 0;
  int removed_blocks_cnt = 0;

  //ia for each row
  for (int row_index = 0; row_index < _num_rows; ++row_index) {

    //ia new row
    IntMatrixBlockPtrMap dest_row;

    IntMatrixBlockPtrMap& src_row = _rows_vector[row_index];
    typename IntMatrixBlockPtrMap::const_iterator src_block_it = src_row.begin();
    typename IntMatrixBlockPtrMap::const_iterator src_row_end = src_row.end();
    while (src_block_it != src_row_end) {
      const int src_col_index = src_block_it->first;

      //ia if there is an entry with that column index, skip
      if (src_col_index == col_idx_) {
        ++removed_blocks_cnt;
        ++src_block_it;
        continue;
      }

      if (src_col_index < col_idx_) {
        new_col_index = src_col_index;
      } else {
        new_col_index = src_col_index - 1;
      }

      dest_row.insert(std::make_pair(new_col_index, src_block_it->second));
      ++src_block_it;
    }
    //ia apply changes
    _rows_vector[row_index] = dest_row;
  }

  --_num_cols;
  _nnz -= removed_blocks_cnt;
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::removeRowAndCol(const int idx_) {
  removeRow(idx_);
  removeCol(idx_);
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::allocateCholesky(SparseBlockMatrix<MatrixBlock>*& cholesky_) const {
  if (_num_rows != _num_cols) {
    throw std::runtime_error("matrix must be squared");
  }

  if (cholesky_) {
    throw std::runtime_error("dest matrix already initialized");
  }

  cholesky_ = new SparseBlockMatrix<MatrixBlock>(_num_rows, _num_cols);
  int ch_nnz = 0; //ia nnz of the cholesky

  long long int prev_add = 0;
  //ia row access pattern
  for (uint64_t row_idx = 0; row_idx < _num_rows; ++row_idx) {
    const IntMatrixBlockPtrMap& src_row = _rows_vector[row_idx];
    //ia check if src row is empty (failure if true)
    if (src_row.empty()) {
      throw std::runtime_error("src row is empty,something is wrong in the source matrix");
    }

    IntMatrixBlockPtrMap& ch_row = cholesky_->_rows_vector[row_idx];
    int starting_col_idx = src_row.begin()->first;

    //ia looping over src cols
    for (uint64_t col_idx = starting_col_idx; col_idx <= row_idx; ++col_idx) {
      // bool empty_block = true;
      bool not_empty = false;

      //ia checking for fill-in
      if (src_row.count(col_idx)) {
        not_empty = true;
        // std::cerr << "r c = " << row_idx << " " << col_idx << std::endl;
        // long long int add = (long long int)src_row.at(col_idx);
        // std::cerr << "source block address = " << YELLOW << add << RESET << std::endl;
        // std::cerr << "delta address = " << RED << add-prev_add << RESET << std::endl;
        // std::cerr << "block\n" << *src_row.at(col_idx) << std::endl;
        // prev_add = add;
        // std::cin.get();
      } else {
        IntMatrixBlockPtrMap& ch_upper_row = cholesky_->_rows_vector[col_idx];
        not_empty = _scalarProdStructure<MatrixBlock>(ch_row, ch_upper_row, col_idx-1);
      }

      if (not_empty) {
        MatrixBlock* ch_new_block = cholesky_->_block_manager.allocateOneBlock(row_idx, col_idx);
        cholesky_->setBlockPtr(row_idx, col_idx, ch_new_block);
        ++ch_nnz;
      }
    }
  }
  cholesky_->_nnz = ch_nnz;
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::computeCholesky(SparseBlockMatrix<MatrixBlock>* cholesky_) const {
  if (!cholesky_->_is_initialized) {
    throw std::runtime_error("dest has not been allocated");
  }

  MatrixBlock accumulator = MatrixBlock::Zero();
  std::vector<MatrixBlock, Eigen::aligned_allocator<MatrixBlock> > inverse_ch_diag_block_vec(_num_rows);

  //ia row access pattern
  for (uint64_t row_idx = 0; row_idx < _num_rows; ++row_idx) {
    const IntMatrixBlockPtrMap& src_row = _rows_vector[row_idx];
    IntMatrixBlockPtrMap& ch_row = cholesky_->_rows_vector[row_idx];

    typename IntMatrixBlockPtrMap::const_iterator src_row_it = src_row.begin();
    typename IntMatrixBlockPtrMap::const_iterator src_row_end = src_row.end();
    typename IntMatrixBlockPtrMap::iterator ch_row_it = ch_row.begin();
    typename IntMatrixBlockPtrMap::iterator ch_row_end = ch_row.find(row_idx+1);

    while (ch_row_it != ch_row_end) {
      const uint64_t col_idx = ch_row_it->first;
      MatrixBlock& ch_block = *(ch_row_it->second);

      ch_block.setZero();
      accumulator.setZero();

      if (src_row_it != src_row_end && src_row_it->first == col_idx) {
        accumulator = *(src_row_it->second);
        ++src_row_it;
      }

      const IntMatrixBlockPtrMap& ch_upper_row = cholesky_->_rows_vector[col_idx];
      accumulator.noalias() -= _scalarProd<MatrixBlock>(ch_row, ch_upper_row, col_idx-1);

      if (row_idx == col_idx) {
        ch_block = accumulator.llt().matrixL();
        inverse_ch_diag_block_vec[row_idx] = ch_block.inverse().transpose();
      } else {
        ch_block = accumulator * inverse_ch_diag_block_vec[col_idx];
      }

      ++ch_row_it;
    }
  }
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::applyPermutation(const IntVector& permutation_vector_) {
  if (_num_rows != _num_cols) {
    throw std::runtime_error("non squared matrix");
  }

  const IntMatrixBlockPtrMapVector aux_view = _rows_vector;
  _rows_vector.clear();
  _rows_vector.resize(_num_rows);
  _nnz = 0;

  for (uint64_t row_idx = 0; row_idx < _num_rows; ++row_idx) {
    const IntMatrixBlockPtrMap& aux_row = aux_view[row_idx];

    typename IntMatrixBlockPtrMap::const_iterator aux_row_it = aux_row.begin();
    typename IntMatrixBlockPtrMap::const_iterator aux_row_end = aux_row.end();

    while (aux_row_it != aux_row_end) {
      uint64_t new_row_idx = permutation_vector_[row_idx];
      uint64_t new_col_idx = permutation_vector_[aux_row_it->first];
      setBlockPtr(new_row_idx, new_col_idx, aux_row_it->second);
      ++aux_row_it;
    }
  }
}

template <class MatrixBlock>
void SparseBlockMatrix<MatrixBlock>::applyInversePermutation(const IntVector& permutation_vector_) {
  if (_num_rows != _num_cols) {
    throw std::runtime_error("non squared matrix");
  }

  _rows_vector.clear();
  _rows_vector.resize(_num_rows);
  _nnz = 0;

  IntVector inverse_permutation(permutation_vector_.size());
  for (uint64_t index = 0; index < permutation_vector_.size(); ++index) {
    inverse_permutation[permutation_vector_[index]] = index;
  }

  const IntMatrixBlockPtrMapVector aux_view = _rows_vector;

  for (uint64_t row_idx = 0; row_idx < _num_rows; ++row_idx) {
    const IntMatrixBlockPtrMap& aux_row = aux_view[row_idx];

    typename IntMatrixBlockPtrMap::const_iterator aux_row_it = aux_row.begin();
    typename IntMatrixBlockPtrMap::const_iterator aux_row_end = aux_row.end();

    while (aux_row_it != aux_row_end) {
      uint64_t new_row_idx = inverse_permutation[row_idx];
      uint64_t new_col_idx = inverse_permutation[aux_row_it->first];
      setBlockPtr(new_row_idx, new_col_idx, aux_row_it->second);
      ++aux_row_it;
    }
  }
}

template <class MatrixBlock>
typename SparseBlockMatrix<MatrixBlock>::IntMatrixBlockPtrMapVector SparseBlockMatrix<MatrixBlock>::_transposedView() const {
  IntMatrixBlockPtrMapVector transposed_view(_num_cols);

  for (uint64_t row_idx = 0; row_idx < _num_rows; ++row_idx) {
    const IntMatrixBlockPtrMap& src_row = _rows_vector[row_idx];

    typename IntMatrixBlockPtrMap::const_iterator src_row_it = src_row.begin();
    typename IntMatrixBlockPtrMap::const_iterator src_row_end = src_row.end();

    while (src_row_it != src_row_end) {
      IntMatrixBlockPtrMap& transp_row = transposed_view[src_row_it->first];
      transp_row.insert(std::make_pair(row_idx, src_row_it->second));
      ++src_row_it;
    }
  }

  return transposed_view;
}

template <class MatrixBlock>
cs* SparseBlockMatrix<MatrixBlock>::csMatrixPattern() const {
  cs* transposed_matrix = cs_spalloc(_num_cols, _num_rows, _nnz, 1, 0);

  int *row_pointers = transposed_matrix->p;
  int* col_indices = transposed_matrix->i;
  double* values = transposed_matrix->x;

  uint64_t counter = 0;
  uint64_t counter_elements = 0;
  for (uint64_t row_idx = 0; row_idx < _num_rows; ++row_idx){
    *row_pointers = counter;
    const IntMatrixBlockPtrMap& src_row = _rows_vector[row_idx];

    typename IntMatrixBlockPtrMap::const_iterator src_row_it = src_row.begin();
    typename IntMatrixBlockPtrMap::const_iterator src_row_end = src_row.end();
    while (src_row_it != src_row_end) {
      uint64_t col_idx = src_row_it->first;
      *values = 1.0;
      *col_indices = col_idx;
      ++counter;
      ++values;
      ++col_indices;
      ++counter_elements;
      ++src_row_it;
    }
    ++row_pointers;
  }
  *row_pointers = counter;
  cs* tm = cs_transpose(transposed_matrix, 1);
  cs_spfree(transposed_matrix);
  return tm;
}


template <class MatrxBlock>
cholmod_sparse* SparseBlockMatrix<MatrxBlock>::cholmodMatrixPattern(cholmod_common* cc_) const {
  cholmod_triplet triplet;

  triplet.itype = CHOLMOD_INT;
  triplet.xtype = CHOLMOD_REAL;
  triplet.dtype = CHOLMOD_DOUBLE;

  triplet.nrow = static_cast<size_t>(_num_rows);
  triplet.ncol = static_cast<size_t>(_num_cols);
  triplet.nzmax = static_cast<size_t>(_nnz);
  triplet.nnz = static_cast<size_t>(_nnz);
  triplet.stype = 1;

  int i_temp[_nnz];
  int j_temp[_nnz];
  double x_temp[_nnz];

  triplet.i = i_temp;
  triplet.j = j_temp;
  triplet.x = x_temp;

  int k = 0;
  for (uint64_t row_idx = 0; row_idx < _num_rows; ++row_idx) {
    const IntMatrixBlockPtrMap& src_row = _rows_vector[row_idx];
    typename IntMatrixBlockPtrMap::const_iterator src_row_it = src_row.begin();
    typename IntMatrixBlockPtrMap::const_iterator src_row_end = src_row.end();
    while (src_row_it != src_row_end) {
      i_temp[k] = src_row_it->first;
      j_temp[k] = row_idx;
      x_temp[k] = 1;
      ++k;
      ++src_row_it;
    }
  }
  cholmod_sparse* ch_sparse= cholmod_triplet_to_sparse(&triplet, triplet.nnz, cc_);
  return ch_sparse;
}


} /* namespace srrg_g2o_slim */
