#pragma once

#include "matrix_block_manager.h"

namespace srrg_g2o_slim {

template <class MatrixBlock>
class SparseBlockMatrix {
public:

  //ia useful typedefs
  typedef std::map<int, MatrixBlock*, std::less<int>, Eigen::aligned_allocator<std::pair<int, MatrixBlock*> > > IntMatrixBlockPtrMap;
  typedef std::vector<IntMatrixBlockPtrMap> IntMatrixBlockPtrMapVector;

  //! @brief default ctor not allowed
  SparseBlockMatrix() = delete;

  //! @brief copy ctor
  //! param[in] other_: matrix to copy
  //! param[in] deep_copy_flag_: if true enables a deep copy
  SparseBlockMatrix(const SparseBlockMatrix<MatrixBlock>& other_,
                    const bool deep_copy_flag_ = false);

  //! @brief ctor from a matrix pull (aka MatrixBlockManager) - matrix does not hold the memory
  //! param[in] num_rows_: number of (block)rows;
  //! param[in] num_cols_: number of (block)cols;
  //! param[in] block_manager_: holds the blocks and manages the memory
  SparseBlockMatrix(const int num_rows_,
                    const int num_cols_,
                    const MatrixBlockManager<MatrixBlock>& block_manager_);

  //! @brief ctor that instanciates a matrix view with a certain number of row and cols
  //! that holds the blocks. The view has zero nnz yet.
  SparseBlockMatrix(const int num_rows_,
                    const int num_cols_);

  //! @brief default dtor - destroys the memory if owned
  virtual ~SparseBlockMatrix();

  //! @brief destroys the view and frees the memory (if owned)
  void reset();

  //! @brief sets all blocks to zero
  void setZero();

  //! @brief sets all block to random blocks
  void setRandom();

  //! @brief sets all block to random blocks
  void setIdentity();

  //! @brief saves the matrix non-zero pattern - octave friendly (row major)
  void savePattern(const std::string& filename_) const;

  //! @brief loads from a txt file (ex generatad from octave - row major)
  void load(const std::string& filename_);

  //! @brief prints the whole matrix block by block - only nnz
  void print() const;

  //! @brief prints the block located at <row_, col_>
  void printBlock(const int row_,
                  const int col_) const;

  //! @brief set a block in the view located at <row_, col_>
  void setBlockPtr(const int row_,
                   const int col_,
                   MatrixBlock* block_ptr_);

  //! @brief get the pointer of the block located at <row_, col_>;
  //! returns nulltptr if it does not exist
  MatrixBlock* getBlockPtr(const int row_,
                           const int col_) const;

  //! @brief creates a block in the view if it does not exist yet and if the matrix hold the memory
  //! returns the pointer to the created block if successful, crashes otherwise
  MatrixBlock* createBlock(const int row_,
                           const int col_);

  //! @brief const/non-const getter of block located at <row_, col_>.
  //! crashes if it does not exist
  const MatrixBlock& at(const int row_,
                        const int col_) const;
  MatrixBlock& at(const int row_,
                  const int col_);

  //! @brief checks whether the block at <row_, col_> is non-zero. returns true if so
  //! (aka if it is not in the view)
  const bool isNonZero(const int row_,
                       const int col_) const;

  //! @brief modifies the view of the matrix removing the selected row
  //! @param[in] row_idx_: index of the row to remove
  void removeRow(const int row_idx_);

  //! @brief modifies the view of the matrix removing the selected col
  //! @param[in] col_idx_: index of the col to remove
  void removeCol(const int col_idx_);

  //! @brief modifies the view of the matrix removing the row AND col specified
  //! @param[in] idx_: index of the row and col to remove
  void removeRowAndCol(const int idx_);

  //! @brief allocates the matrix resulting from the Cholesky decomposition of
  //! this matrix.
  //! @param[in] cholesky_: the dest matrix; it must not be initilized. It owns the memory
  void allocateCholesky(SparseBlockMatrix<MatrixBlock>*& cholesky_) const;

  //! @brief computes the Cholesky decomposition of this matrix and put
  //! the result in the argument.
  //! @param[in] cholesky_: the dest matrix; it must be already allocated with allocateCholesky
  void computeCholesky(SparseBlockMatrix<MatrixBlock>* cholesky_) const;

  //! @brief applyes a permutation specified as argument
  //! to the vector view. Works only with squared matrices
  void applyPermutation(const IntVector& permutation_vector_);

  //ia TODO lo famo schioppa'?
  //! @brief applyes the inverse permutation specified as argument to
  //! the vector view. Works only with squared matrices
  void applyInversePermutation(const IntVector& permutation_vector_);

  //! @brief builds a cs matrix filled with the matrix pattern
  //! it is required to compute ordinations with the suitesparse lib
  cs* csMatrixPattern() const;

  //! @brief builds a cholmod_sparse matrix filled with the matrix pattern
  //! it is required to compute ordinations with cholmod
  cholmod_sparse* cholmodMatrixPattern(cholmod_common* cc_) const;

  //! @brief checks if the matrix is squared.
  inline bool isSquared() {
    return _num_rows == _num_cols;
  }

  //! @brief destroys the view only, memory remains untouched.
  //! this can be called only if matrix do not owns the memory.
  inline void resetView() {
    _rows_vector.clear();
    _rows_vector.resize(_num_rows);
    _nnz = 0;
  }

  //! @brief inline getters
  inline const int rows() const {return _num_rows;}
  inline const int cols() const {return _num_cols;}
  inline const uint64_t& nnz() const {return _nnz;}
  inline const bool hasStorage() const {return _has_storage;}
  inline const bool isInitialized() const {return _is_initialized;}
  inline const float sparsity() const {return 1.0-(_nnz/(float)(_num_rows*_num_cols));}

  //todo erase
  inline const IntMatrixBlockPtrMapVector& rowsV() const {return _rows_vector;}
  inline IntMatrixBlockPtrMapVector& rowsV() {return _rows_vector;}
  inline uint64_t& nnz() {return _nnz;}

protected:

  //! @brief returns a transposed view of the matrix. The actual blocks must
  //! be transposed when accessed.
  IntMatrixBlockPtrMapVector _transposedView() const;

  int _num_rows;
  int _num_cols;

  uint64_t _nnz;  //ia number of non-zero blocks

  bool _has_storage;
  bool _is_initialized;

  IntMatrixBlockPtrMapVector _rows_vector;  //ia vector of rows; each row
                                            //ia is a map <col_index, block_ptr>

  //ia TODO don't know if it could be useful to have this shit here always ready
//  IntMatrixBlockPtrMapVector _transposed_view; //ia transposed view of the matrix.
//                                               //ia When you access a block you have to transpose it.

  MatrixBlockManager<MatrixBlock> _block_manager;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  template<class M, class V>
  friend class LinearSolver;
};

} //ia end of the namespace srrg_g2o_slim

#include "sparse_block_matrix.hpp"

