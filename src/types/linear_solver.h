#pragma once
#include "sparse_block_matrix.h"
#include "dense_block_vector.h"

namespace srrg_g2o_slim {

enum OrderingType {
  PLAIN_ORDERING = 0,
  REVERSING_ORDERING = 1,
  SUITESPARSE_AMD_ORDERING = 2,
  CHOLMOD_METIS_ORDERING = 3,
  CHOLMOD_NESDIS_ORDERING = 4,
  CHOLMOD_COLAMD_ORDERING = 5,
  IDENTITY_ORDERING = 6,
  CUSTOM_ORDERING = 7
};

template <class MatrixType, class VectorType>
class LinearSolver {
public:

  LinearSolver();
  virtual ~LinearSolver();

  inline void setPermutation(const IntVector& permutation_) {
    _permutation_vector = permutation_;
    _ordering = CUSTOM_ORDERING;
  }

  inline const IntVector& permutation() const {
    return _permutation_vector;
  }

  inline void setOrdering(const OrderingType ordering_) {
    _ordering = ordering_;
  }

  inline const OrderingType& ordering() const {
    return _ordering;
  }

  inline void setMatrix(SparseBlockMatrix<MatrixType>* H_) {
    _H = H_;
  }

  inline void setRightHandSideVector(DenseBlockVector<VectorType>* b_) {
    _b = b_;
  }

  void printStats() const;

  void init();
  void compute(DenseBlockVector<VectorType>* dx_);

protected:
  void _forwardSub(SparseBlockMatrix<MatrixType>* src_mat_,
                   DenseBlockVector<VectorType>* src_vec_,
                   DenseBlockVector<VectorType>* res_vec_,
                   const bool transpose_matrix_);

  void _backwardSub(SparseBlockMatrix<MatrixType>* src_mat_,
                    DenseBlockVector<VectorType>* src_vec_,
                    DenseBlockVector<VectorType>* res_vec_,
                    const bool transpose_matrix_);

  //ia owned by the linear solver
  SparseBlockMatrix<MatrixType>* _ch_L = nullptr;
  DenseBlockVector<VectorType>* _y = nullptr;

  //ia NOT owned by the linear solver
  SparseBlockMatrix<MatrixType>* _H = nullptr;
  DenseBlockVector<VectorType>* _b = nullptr;

  IntVector _permutation_vector;
  OrderingType _ordering;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

} //ia end namespace srrg_g2o_slim

#include "linear_solver.hpp"
