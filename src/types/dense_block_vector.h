#pragma once

#include "vector_block_manager.h"

namespace srrg_g2o_slim {

template <class BlockType>
class DenseBlockVector {
public:

  //ia useful typedef
  typedef std::map<int, BlockType*, std::less<int>, Eigen::aligned_allocator<std::pair<int, BlockType*> > > IntVectorBlockPtrMap;

  //! @brief default ctor - vector holds the memory
  DenseBlockVector();

  //! @brief copy ctor
  //! param[in] other_: vector to copy
  //! param[in] deep_copy_flag_: if true enables a deep copy of other_
  DenseBlockVector(const DenseBlockVector<BlockType>& other_,
                   const bool deep_copy_flag_ = false);

  //! @brief ctor from a vector pull (aka a VectorBlockManager)
  DenseBlockVector(const VectorBlockManager<BlockType>& block_manager_);

  //! @brief ctor of a vector of a certain size, all blocks set to zero
  //! vector owns the blocks storage
  DenseBlockVector(const int number_of_blocks_);

  //! @brief default dtor
  virtual ~DenseBlockVector();

  //! @brief destroys the view and frees the memory (if owned)
  void reset();

  //! @brief sets all blocks to zero
  void setZero();

  //! @brief sets all block to random blocks
  void setRandom();

  //! @brief saves the vector view in a txt file (octave friendly)
  void save(const std::string& filename_) const;

  //! @brief prints the whole vector block by block
  void print() const;

  //! @brief prints the block with index idx_
  void printBlock(const int idx_) const;

  //! @brief returns the block at index idx_
  const BlockType& at(const int idx_) const;
  BlockType& at(const int idx_);

  //! @brief set a block in the view with index idx_
  void setBlockPtr(const int idx_, BlockType* block_ptr_);

  //! @brief get a block in the view with index idx_
  BlockType* getBlockPtr(const int idx_);

  //! @brief create a block if it does not exists yet and only if the vector owns the blocks
  //! @brief returns the pointer of the created block if it is successful, crashes otherwise
  BlockType* createBlock(const int idx_);

  //! @brief applyes a permutation specified as argument
  //! to the vector view
  void applyPermutation(const IntVector& permutation_vector_);

  //ia TODO lo famo schioppa'?
  //! @brief applyes the inverse permutation specified as argument to
  //! the vector view
  void applyInversePermutation(const IntVector& permutation_vector_);

  //! @brief modifies the view of the vector removing the selected index
  //! @param[in] index_: index of the block to remove
  void removeBlock(const int idx_);

  //! @brief inline getters
  const bool initialized() const {return _is_initialized;}
  const bool hasStorage() const {return _has_storage;}
  const int size() const {return _size;}

protected:
  int       _size;
  bool      _has_storage;
  bool      _is_initialized;

  IntVectorBlockPtrMap            _block_view;
  VectorBlockManager<BlockType>   _block_manager;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


} //ia end of the namespace

#include "dense_block_vector.hpp"
