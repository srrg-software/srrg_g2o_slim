namespace srrg_g2o_slim {

template <class MatrixType, class VectorType>
BaseOptimizer<MatrixType, VectorType>::BaseOptimizer() {
  _iterations = 0;
  _kernel_width = 0;
  _total_chi = 0;
  _inliers = 0;
  _outliers = 0;
  _verbose = false;
}

template <class MatrixType, class VectorType>
BaseOptimizer<MatrixType, VectorType>::~BaseOptimizer() {}

} //ia namespace srrg_g2o_slim
