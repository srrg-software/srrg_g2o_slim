namespace srrg_g2o_slim {

template <class EstimateType>
BaseVertex<EstimateType>::BaseVertex(const uint64_t& id_,
                                     const uint64_t& vertex_index_,
                                     const VertexMinimalDim& vertex_dimension_,
                                     const EstimateType& estimate_,
                                     const bool fixed_) :
                                            _id(id_),
                                            _index(vertex_index_),
                                            _dimension(vertex_dimension_) {
  _estimate = estimate_;
  _fixed = fixed_;
}

template <class EstimateType>
BaseVertex<EstimateType>::~BaseVertex(){
}

template <class EstimateType>
BaseVertex<EstimateType>* BaseVertex<EstimateType>::clone() const {
  BaseVertex<EstimateType>* cloned_vertex = new BaseVertex<EstimateType>(_id, _index,
                                                                         _dimension, _estimate, _fixed);
  return cloned_vertex;
}

} //ia end of the namespace
