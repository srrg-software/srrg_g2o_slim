#include "block_manager.h"

namespace srrg_g2o_slim {

  BlockManager::BlockManager() {
    _dimension = 0;
  }

  BlockManager::BlockManager(const uint64_t& number_of_blocks_) {
    _dimension = number_of_blocks_;
  }

  BlockManager::~BlockManager() {}
}//ia end of the namespace
