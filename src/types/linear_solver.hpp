#include "utilities/system_utils.h"
namespace srrg_g2o_slim {

void _printPermutation(const IntVector& p_) {
  std::cerr << "permutation:" << std::endl;
  for (int i = 0; i < p_.size(); ++i) {
    std::cerr << p_[i] << "\t";
  }
  std::cerr << std::endl;
}

template <class MatrixType>
IntVector _computeIdentityPermutation(SparseBlockMatrix<MatrixType>* matrix_) {
  const int& size = matrix_->rows();
  IntVector p_vec(size);

  for (int i = 0; i < size; ++i) {
    p_vec[i] = i;
  }

  return p_vec;
}

template <class MatrixType>
IntVector _computeReversingPermutation(SparseBlockMatrix<MatrixType>* matrix_) {
  const int& size = matrix_->rows();
  IntVector p_vec(size);

  for (int i = 0; i < size; ++i) {
    p_vec[i] = i;
  }

  std::reverse(p_vec.begin(), p_vec.end());

  return p_vec;
}

template <class MatrixType>
IntVector _computeSuiteSparsePermutation(SparseBlockMatrix<MatrixType>* matrix_) {
  const int& size = matrix_->rows();
  IntVector p_vec(size);

  cs* cs_matrix = matrix_->csMatrixPattern();

  int* amd_inv_permutation_array = cs_amd(1,cs_matrix);
  for (int i = 0; i < size; ++i) {
    p_vec[amd_inv_permutation_array[i]] = i;
  }

  cs_spfree(cs_matrix);
  cs_free(amd_inv_permutation_array);

  return p_vec;
}

template <class MatrixType>
IntVector _computeCholmodPermutation(SparseBlockMatrix<MatrixType>* matrix_,
                                     int cholmod_ordering_method_) {
  cholmod_common cc;
  cholmod_start(&cc);

  cc.nmethods = 1;
  cc.method[0].ordering = cholmod_ordering_method_;
  cc.supernodal = CHOLMOD_SUPERNODAL;

  cholmod_sparse* ch_sparse_matrix = matrix_->cholmodMatrixPattern(&cc);
  cholmod_factor* factors = cholmod_analyze(ch_sparse_matrix, &cc);

  IntVector p_vec(factors->n);

  int* cholmod_inv_perm_array = (int*)factors->Perm;
  for (int i = 0; i < factors->n; ++i) {
    p_vec[cholmod_inv_perm_array[i]]=i;
  }

  cholmod_free_factor(&factors, &cc);
  cholmod_free_sparse(&ch_sparse_matrix, &cc);
  cholmod_finish(&cc);

  return p_vec;
}


template <class MatrixType, class VectorType>
LinearSolver<MatrixType, VectorType>::LinearSolver() {
  _ordering = PLAIN_ORDERING;
}


template <class MatrixType, class VectorType>
LinearSolver<MatrixType, VectorType>::~LinearSolver() {
  delete _ch_L;
  delete _y;
}


template <class MatrixType, class VectorType>
void LinearSolver<MatrixType, VectorType>::printStats() const {
  if (!_H || !_b) {
    throw std::runtime_error("you must set the matrix and the rhs vector before");
  }

  std::cerr << std::endl;
  std::cerr << "matrix" << std::endl;
  std::cerr << "rows = " << _H->rows() << "\tcols = " << _H->cols() << "\tnnz = " << _H->nnz() << "\tsparsity = " << _H->sparsity() << std::endl;
  std::cerr << "right hand side vector" << std::endl;
  std::cerr << "size = " << _b->size() << std::endl;
  
  if (_ch_L) {
    std::cerr << "cholesky" << std::endl;
    std::cerr << "rows = " << _ch_L->rows() << "\tcols = " << _ch_L->cols() << "\tnnz = " << _ch_L->nnz() << "\tsparsity = " << _ch_L->sparsity() << std::endl;
  }
  
  std::cerr << std::endl;
}


template <class MatrixType, class VectorType>
void LinearSolver<MatrixType, VectorType>::init() {
  if (!_H || !_b) {
    throw std::runtime_error("you must set the matrix and the rhs vector before initialize");
  }

  switch (_ordering) {
    case SUITESPARSE_AMD_ORDERING:
      _permutation_vector = _computeSuiteSparsePermutation(_H);
      break;
    case IDENTITY_ORDERING:
      _permutation_vector = _computeIdentityPermutation(_H);
      break;
    case REVERSING_ORDERING:
      _permutation_vector = _computeReversingPermutation(_H);
      break;
    case CHOLMOD_NESDIS_ORDERING:
    case CHOLMOD_METIS_ORDERING:
    case CHOLMOD_COLAMD_ORDERING:
      _permutation_vector = _computeCholmodPermutation(_H, _ordering);
      break;
    case PLAIN_ORDERING:
      _permutation_vector.clear();
      break;
    case CUSTOM_ORDERING:
      break;
    default:
      throw std::runtime_error("unknown ordering");
      break;
  }

  if (_permutation_vector.size()) {
    _H->applyPermutation(_permutation_vector);
  }

  //ia allocate ch
  _H->allocateCholesky(_ch_L);
  //ia allocate y
  _y = new DenseBlockVector<VectorType>(_b->size());
}


template <class MatrixType, class VectorType>
void LinearSolver<MatrixType, VectorType>::compute(DenseBlockVector<VectorType>* result_) {
  //ia zero
  result_->setZero();

  //ia compute ch
  double t0 = getTime();
  _H->computeCholesky(_ch_L); //OK
  double delta_cholesky = getTime() - t0;
  if (_permutation_vector.size() != 0) {
    _b->applyPermutation(_permutation_vector);
    _y->applyPermutation(_permutation_vector);
    result_->applyPermutation(_permutation_vector);
  }

  t0 = getTime();
  _forwardSub(_ch_L, _b, _y, false);
  double delta_sub_fwd = getTime() - t0;
  t0 = getTime();
  _backwardSub(_ch_L, _y, result_, true);
  double delta_sub_bwd = getTime() - t0;

  if (_permutation_vector.size() != 0) {
    result_->applyInversePermutation(_permutation_vector);
    _y->applyInversePermutation(_permutation_vector);
    _b->applyInversePermutation(_permutation_vector);
  }

  std::cerr << "CHOL TIME = " << delta_cholesky << std::endl;
  std::cerr << "FWD SUBS TIME = " << delta_sub_fwd << std::endl;
  std::cerr << "BWD SUBS TIME = " << delta_sub_bwd << std::endl;

  _y->setZero();
}


template <class MatrixType, class VectorType>
void LinearSolver<MatrixType, VectorType>::_forwardSub(SparseBlockMatrix<MatrixType>* src_mat_,
                                                       DenseBlockVector<VectorType>* src_vec_,
                                                       DenseBlockVector<VectorType>* res_vec_,
                                                       const bool transpose_matrix_) {
  //ia sanity checks
  if (src_mat_->rows() != src_vec_->size() ||
      src_vec_->size() != res_vec_->size()) {
    throw std::runtime_error("dimension mismatch");
  }

  if (!res_vec_->initialized()) {
    throw std::runtime_error("result vector not initialized");
  }

  typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMapVector matrix_view;
  if (transpose_matrix_) {
    matrix_view = src_mat_->_transposedView();
  } else {
    matrix_view = src_mat_->_rows_vector;
  }

  //ia computation
  MatrixType src_block = MatrixType::Zero();
  for (uint64_t row_idx = 0; row_idx < src_mat_->rows(); ++row_idx) {
    VectorType result_block = src_vec_->at(row_idx);

    typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap& src_row = matrix_view[row_idx];
    typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap::const_iterator src_row_it = src_row.begin();
    typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap::const_iterator src_row_end = src_row.find(row_idx);

    while (src_row_it != src_row_end) {

      if(transpose_matrix_) {
        src_block = src_row_it->second->transpose();
      } else {
        src_block = *(src_row_it->second);
      }

      result_block.noalias() -= src_block * res_vec_->at(src_row_it->first);
      ++src_row_it;
    }

    if (transpose_matrix_) {
      result_block = src_row[row_idx]->transpose().inverse() * result_block; //ia unguarded access requires non const src_row
    } else {
      result_block = src_row[row_idx]->inverse() * result_block; //ia unguarded access requires non const src_row
    }

    //ia set
    res_vec_->at(row_idx) = result_block;
  }
}


template <class MatrixType, class VectorType>
void LinearSolver<MatrixType, VectorType>::_backwardSub(SparseBlockMatrix<MatrixType>* src_mat_,
                                                        DenseBlockVector<VectorType>* src_vec_,
                                                        DenseBlockVector<VectorType>* res_vec_,
                                                        const bool transpose_matrix_) {
  //ia sanity checks
  if (src_mat_->rows() != src_vec_->size() ||
      src_vec_->size() != res_vec_->size()) {
    throw std::runtime_error("dimension mismatch");
  }

  if (!res_vec_->initialized()) {
    throw std::runtime_error("result vector not initialized");
  }

  typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMapVector matrix_view;
  if (transpose_matrix_) {
    matrix_view = src_mat_->_transposedView();
  } else {
    matrix_view = src_mat_->_rows_vector;
  }


  //ia compute
  MatrixType src_block = MatrixType::Zero();
  for (int64_t row_idx = src_mat_->rows()-1; row_idx >= 0; --row_idx) {
    VectorType result_block = src_vec_->at(row_idx);

    typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap& src_row = matrix_view[row_idx];
    typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap::const_iterator src_row_it = src_row.find(row_idx);
    typename SparseBlockMatrix<MatrixType>::IntMatrixBlockPtrMap::const_iterator src_row_end = src_row.end();

    while (src_row_it != src_row_end) {

      if(transpose_matrix_) {
        src_block = src_row_it->second->transpose();
      } else {
        src_block = *(src_row_it->second);
      }

      result_block.noalias() -= src_block * res_vec_->at(src_row_it->first);
      ++src_row_it;
    }

    if (transpose_matrix_) {
      result_block = src_row[row_idx]->transpose().inverse() * result_block; //ia unguarded access requires non const src_row
    } else {
      result_block = src_row[row_idx]->inverse() * result_block; //ia unguarded access requires non const src_row
    }

    //ia set
    res_vec_->at(row_idx) = result_block;
  }
}


} //ia end namespace srrg_g2o_slim
