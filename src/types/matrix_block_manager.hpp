namespace srrg_g2o_slim {

template <class MatrixBlockType>
MatrixBlockManager<MatrixBlockType>::MatrixBlockManager() :
                                      BlockManager() {
  //ia default ctor
}


template <class MatrixBlockType>
MatrixBlockManager<MatrixBlockType>::MatrixBlockManager(const MatrixBlockManager<MatrixBlockType>& other_) {
  typename IntPairBlockPtrMap::const_iterator block_it = other_._blocks_map.begin();
  while (block_it != other_._blocks_map.end()) {
    Block* block = new Block();
    *block = *(block_it->second);
    _blocks_map.insert(std::make_pair(block_it->first, block));
    ++block_it;
  }

  _dimension = other_._dimension;

  //ia check for the right number of blocks
  if (_blocks_map.size() != other_._dimension) {
    throw std::runtime_error("Number of allocated blocks does not match, exiting");
  }
}


template <class MatrixBlockType>
MatrixBlockManager<MatrixBlockType>::MatrixBlockManager(const std::vector<IntPair>& indexes_vector_) {
  for (uint64_t i = 0; i < indexes_vector_.size(); ++i) {
    const IntPair& block_indexes = indexes_vector_[i];

    if (!_blocks_map.count(block_indexes)) {
      Block* block = new Block();
      block->setIdentity();
      _blocks_map.insert(std::make_pair(block_indexes, block));
    } else {
      _blocks_map[block_indexes]->setIdentity();
    }
  }

  _dimension = _blocks_map.size();
}


template <class MatrixBlockType>
MatrixBlockManager<MatrixBlockType>::~MatrixBlockManager() {
  typename IntPairBlockPtrMap::iterator block_it = _blocks_map.begin();
  while (block_it != _blocks_map.end()) {
    delete block_it->second;
    ++block_it;
  }
}


template <class MatrixBlockType>
void MatrixBlockManager<MatrixBlockType>::reset() {
  typename IntPairBlockPtrMap::iterator block_it = _blocks_map.begin();
  while (block_it != _blocks_map.end()) {
    delete block_it->second;
    ++block_it;
  }

  _blocks_map.clear();
  _dimension = 0; 
}


template <class MatrixBlockType>
void MatrixBlockManager<MatrixBlockType>::setZero() {
  typename IntPairBlockPtrMap::iterator block_it = _blocks_map.begin();
  while (block_it != _blocks_map.end()) {
    block_it->second->setZero();
    ++block_it;
  }
}


template <class MatrixBlockType>
MatrixBlockType* MatrixBlockManager<MatrixBlockType>::allocateOneBlock(const int r_,
                                                                       const int c_) {
  const IntPair block_indexes(r_, c_);
  if (!_blocks_map.count(block_indexes)) {
    Block* block = new Block();
    block->setIdentity();
    _blocks_map.insert(std::make_pair(block_indexes, block));
    ++_dimension;
  }
  
  return _blocks_map[block_indexes];
}


}//ia end of the namespace
