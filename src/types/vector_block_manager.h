#pragma once

#include "block_manager.h"

namespace srrg_g2o_slim {

  template <class VectorBlockType>
  class VectorBlockManager : public BlockManager {
  public:

    //ia useful typedefs
    typedef VectorBlockType Block;
    typedef std::map<uint64_t, Block*, std::less<uint64_t>, Eigen::aligned_allocator<std::pair<uint64_t, Block*> > > IntVectorBlockPtrMap;

    //ia ctor
    VectorBlockManager();

    //! @brief deep copy of the other_ block_manager
    VectorBlockManager(const VectorBlockManager<VectorBlockType>& other_);

    //! @brief allocate a certain number of blocks and sets them to zero
    //! @param[in] dimension_: number of blocks to allocate
    VectorBlockManager(const uint64_t& dimension_);

    //ia dtor - deletes the blocks and frees the memory
    virtual ~VectorBlockManager();

    //! @brief deletes the blocks and frees the memory
    void reset();

    //! @brief sets all blocks to zero
    void setZero();

    //! @brief allocates a certain number of blocks and sets them to zero
    //! param[in] number_of_blocks_: how many blocks we want to allocate
    void allocate(const uint64_t& number_of_blocks_);

    //! @brief allocates just one block and sets it to zero;  
    //! returns the pointer of the created block
    //! @param[in] index_: the index of the block in the memory map
    VectorBlockType* allocateOneBlock(const uint64_t& index_);

    //! @brief returns the block at index = idx_
    inline const Block& at(const uint64_t& idx_) const {return *_blocks_map.at(idx_);}
    inline Block& at(const uint64_t& idx_) {return *_blocks_map.at(idx_);}

    //! @brief returns the pointer to the block at index = idx_
    inline Block* getBlockPtr(const uint64_t& idx_) const {return _blocks_map.at(idx_);}

    //! @brief return the whole pull of blocks
    inline const IntVectorBlockPtrMap& getBlocksMap() const {return _blocks_map;}
    inline IntVectorBlockPtrMap& getBlocksMap() {return _blocks_map;}


  protected:
    //ia blocks pull
    IntVectorBlockPtrMap _blocks_map;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  };

  
}//ia end of the namespace

#include "vector_block_manager.hpp"
