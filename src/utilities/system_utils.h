#pragma once

#include <cstdint>
#include <cmath>
#include <sys/time.h>
#include <sys/resource.h>

namespace srrg_g2o_slim {

//! @brief converts a system timeval to seconds
//! @return converts a system timeval in seconds
inline double tv2sec(const struct timeval& tv_) {
  return (static_cast<double>(tv_.tv_sec)+1e-6*static_cast<double>(tv_.tv_usec));
}

//! @brief returns the system time in seconds
//! @return the system time in seconds
double getTime();

//! @brief check if an Eigen type contains a nan element
//! @returns true if at least one element of
//! the argument is null
template <class T>
inline bool isNan(const T& m) {
  for (uint64_t i = 0; i < m.rows(); i++) {
    for (uint64_t j = 0; j < m.cols(); j++) {
      double v = m(i,j);
      if (std::isnan(v))
        return true;
    }
  }
  return false;
}

//ia time macros
#define SRRG_G2O_SLIM_CREATE_CHRONOMETER(NAME) \
protected: srrg_g2o_slim::Real _time_consumption_seconds_##NAME = 0; \
public: const srrg_g2o_slim::Real getTimeConsumptionSeconds_##NAME() const {return _time_consumption_seconds_##NAME;}
#define SRRG_G2O_SLIM_CHRONOMETER_START(NAME) const g2o_slim::Real time_start_seconds_##NAME = srrg_g2o_slim::getTime();
#define SRRG_G2O_SLIM_CHRONOMETER_STOP(NAME) _time_consumption_seconds_##NAME += srrg_g2o_slim::getTime()-time_start_seconds_##NAME;
#define SRRG_G2O_SLIM_CHRONOMETER_RESET(NAME) _time_consumption_seconds_##NAME = 0;

}//ia end of the namespace

