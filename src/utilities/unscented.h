#pragma once

#include "definitions.h"
#include "geometry_utils.h"

namespace  srrg_g2o_slim {

template <class S>
struct SigmaPoint {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  SigmaPoint(const S sample_, double wi_, double wp_) :
    sample(sample_), wi(wi_), wp(wp_) {}
  
  SigmaPoint() :
    wi(0), wp(0) {}

  S sample;
  double wi;
  double wp;
};


//ia useful typedefs
typedef SigmaPoint<Vector6> SigmaPoint6;
typedef SigmaPoint<Vector12> SigmaPoint12;
typedef std::vector<SigmaPoint6, Eigen::aligned_allocator<SigmaPoint6> > SigmaPoint6Vector;
typedef std::vector<SigmaPoint12, Eigen::aligned_allocator<SigmaPoint12> > SigmaPoint12Vector;


template <class S, class C>
void sampleUnscented(const S& mean_, 
                     const C& covariance_,
                     std::vector<SigmaPoint<S>, Eigen::aligned_allocator<SigmaPoint<S> > >& sigma_points_) {

  const int dim         = mean_.size();
  const int num_points  = 2 * dim + 1;

  const double alpha  = 1e-3;
  const double beta   = 2.0;
  const double lambda = alpha * alpha * dim;
  const double wi     = 1./(2.0 * (dim + lambda) );

  sigma_points_.resize(num_points);

  //! First point
  const double wi_0 = lambda/(dim + lambda);
  const double wp_0 = wi_0 + (1.0 - alpha * alpha * beta);
  sigma_points_[0] = SigmaPoint<S>(mean_, wi_0, wp_0);

  Eigen::LLT<C> chol;
  chol.compute((dim + lambda) * covariance_);
  if (chol.info() == Eigen::NumericalIssue)
    throw std::runtime_error("Numerical troubles occured while computing the Cholesky decomposition");

  const C& L = chol.matrixL();

  uint8_t k = 1;
  for (uint8_t i = 0; i < dim; ++i) {
    S s(L.col(i));
    sigma_points_[k++] = SigmaPoint<S>(mean_ + s, wi, wi);
    sigma_points_[k++] = SigmaPoint<S>(mean_ - s, wi, wi);
  }
}


template <class S, class C>
void reconstructGaussian(const std::vector<SigmaPoint<S>, Eigen::aligned_allocator<SigmaPoint<S> > >& sigma_points_,
                         S& mean_, 
                         C& covariance_) {

  mean_.setZero();
  covariance_.setZero();

  for (int i = 0; i < sigma_points_.size(); ++i) {
    const SigmaPoint<S>& p = sigma_points_[i];
    mean_.noalias() += p.wi * p.sample;
  }

  for (int i = 0; i < sigma_points_.size(); ++i) {
    const SigmaPoint<S>& p = sigma_points_[i];
    S delta = p.sample - mean_;
    covariance_.noalias() += p.wp * (delta * delta.transpose());
  }
}

} //ia end of namespace srrg_g2o_slim