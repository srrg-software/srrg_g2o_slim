#include "optimizer_3d.h"
#include "graph_3d.h"
#include "utilities/system_utils.h"

namespace srrg_g2o_slim {

Optimizer3D::Optimizer3D() : BaseOptimizer<Matrix6, Vector6>() {
  //ia create the block managers
  _hessian_block_manager = new MatrixBlockManager6();
  _rhs_block_manager = new VectorBlockManager6();
  _update_block_manager = new VectorBlockManager6();
  _initialized = false;

  //ia cerr settings
  std::cerr << std::fixed << std::setprecision(4);
}


Optimizer3D::~Optimizer3D() {
  //ia checkout stuff
  delete _update;
  delete _rhs;
  delete _hessian;
  delete _update_block_manager;
  delete _rhs_block_manager;
  delete _hessian_block_manager;
}


void Optimizer3D::clear() {
  //ia checkout stuff, ready to be re-initialized
  delete _update;
  delete _rhs;
  delete _hessian;

  _update_block_manager->reset();
  _rhs_block_manager->reset();
  _hessian_block_manager->reset();
  
  _initialized = false;
}


void Optimizer3D::init() {
  double t0 = getTime();
  //ia checks to understand whether you are a moron or a cool guy
  if (_initialized) {
    throw std::runtime_error("optimizer already initialized -> call clear");
  }

  if (!_graph_ptr) {
    throw std::runtime_error("no graph");
  }

  if (!_solver_ptr) {
    throw std::runtime_error("no solver");
  }

  const uint64_t& num_vertices = _graph_ptr->numVertices();
  const uint64_t& num_edges = _graph_ptr->numEdges();
  if (num_vertices == 0 || num_edges == 0) {
    throw std::runtime_error("empty graph");
  }

  //ia take the things
  Graph3D* graph_3D = static_cast<Graph3D*>(_graph_ptr);
  const IntVertexSE3PtrMap& vertices = graph_3D->vertices();
  const EdgeSE3PtrVector& edges = graph_3D->edges();

  //ia allocate stuff
  for (uint64_t edge_idx = 0; edge_idx < num_edges; ++edge_idx) {
    EdgeSE3* e = edges[edge_idx];

    const uint64_t& v_from_index = e->vertexAssociation().from_ptr->index();
    const uint64_t& v_to_index = e->vertexAssociation().to_ptr->index();

    _hessian_block_manager->allocateOneBlock(v_from_index, v_from_index);
    _hessian_block_manager->allocateOneBlock(v_from_index, v_to_index);
    _hessian_block_manager->allocateOneBlock(v_to_index, v_from_index);
    _hessian_block_manager->allocateOneBlock(v_to_index, v_to_index);
    
    _rhs_block_manager->allocateOneBlock(v_from_index);
    _rhs_block_manager->allocateOneBlock(v_to_index);

    _update_block_manager->allocateOneBlock(v_from_index);
    _update_block_manager->allocateOneBlock(v_to_index);
  }

  //ia create views on top of the managers
  _hessian = new SparseBlockMatrix6(num_vertices, num_vertices, *_hessian_block_manager);
  _rhs = new DenseBlockVector6(*_rhs_block_manager);
  _update = new DenseBlockVector6(*_update_block_manager);

  //ia remove fixed vertices' contribution to the optimization process
  std::set<int> removed_vertexes;

  typename IntVertexSE3PtrMap::const_iterator v_it = vertices.begin();
  typename IntVertexSE3PtrMap::const_iterator v_end = vertices.end();
  int counter = 0;
  while (v_it != v_end) {
    if (!v_it->second->fixed()) {
      ++v_it;
      continue;
    }

    const uint64_t& vertex_index = v_it->second->index();
    removed_vertexes.insert(vertex_index);
    int idx_to_remove = vertex_index - counter++;

    _hessian->removeRowAndCol(idx_to_remove);
    _rhs->removeBlock(idx_to_remove);
    _update->removeBlock(idx_to_remove);

    ++v_it;
  }

  //ia initialize the linear solver
  _solver_ptr->setMatrix(_hessian);
  _solver_ptr->setRightHandSideVector(_rhs);
  _solver_ptr->init();

  //ia zero the Hessian
  _hessian_block_manager->setZero();

  double delta_time = getTime() - t0;

  if (_verbose) {
    _solver_ptr->printStats();
    std::cerr << "INIT TIME = " << delta_time << std::endl;
  }
}


void Optimizer3D::optimize() {
  double t0 = 0, t1 = 0;
  double delta_step = 0, delta_update = 0;

  for (uint16_t i = 0; i < _iterations; ++i) {
    t0 = getTime();
    _oneStep();
    t1 = getTime();
    delta_step = t1-t0;

    t0 = getTime();
    _applyUpdate();
    t1 = getTime();
    delta_update = t1-t0;

    if (_verbose) {
      std::cerr << "STEP TIME = " << delta_step << "\tUPDATE TIME = " << delta_update << std::endl;
      std::cerr << "TOT CHI2 = " << YELLOW << _total_chi << RESET << std::endl;
    }
  }

  std::cerr << std::endl;
}


void Optimizer3D::_oneStep() {
  _total_chi = 0;

  //ia take the things
  const uint64_t& num_vertices = _graph_ptr->numVertices();
  const uint64_t& num_edges = _graph_ptr->numEdges();

  Graph3D* graph_3D = static_cast<Graph3D*>(_graph_ptr);
  const EdgeSE3PtrVector& edges = graph_3D->edges();

  double t0 = getTime();
  for (uint64_t edge_idx = 0; edge_idx < num_edges; ++edge_idx) {
    EdgeSE3* e = edges[edge_idx];

    const uint64_t& v_from_index = e->vertexAssociation().from_ptr->index();
    const uint64_t& v_to_index = e->vertexAssociation().to_ptr->index();

    e->computeError();
    e->computeJacobians();

    const Matrix12_6& Ji = e->jacobianXi();
    Vector12 error = e->error();
    Matrix12 omega = e->information();

    // std::cerr << BOLDRED << "edge <" << v_from_index << ", " << v_to_index << ">" << std::endl << RESET;
    // std::cerr << "error = " << error.transpose() << std::endl;
    // std::cerr << "jacobian_i" << std::endl << Ji << std::endl;
    // std::cerr << "omega" << std::endl << omega << std::endl;
    // std::cin.get();

    //ia Robust kernel
    Real chi = error.transpose() * omega * error;
    _kernelize(chi, omega, error);

    _total_chi  += chi;

    Matrix6 Jt_O_J = Ji.transpose() * omega * Ji;
    Vector6 Jt_O_e = Ji.transpose() * omega * error;

    _hessian_block_manager->at(v_from_index, v_from_index).noalias()  += Jt_O_J;
    _hessian_block_manager->at(v_to_index, v_to_index).noalias()      += Jt_O_J;
    _hessian_block_manager->at(v_from_index, v_to_index).noalias()    -= Jt_O_J;
    _hessian_block_manager->at(v_to_index, v_from_index).noalias()    -= Jt_O_J;

    _rhs_block_manager->at(v_from_index).noalias()  += Jt_O_e;
    _rhs_block_manager->at(v_to_index).noalias()    -= Jt_O_e;
  }
  double t1 = getTime();

  if (_verbose) {
    std::cerr << "LIN TIME = " << t1-t0 << std::endl;
  }

  //ia do the cholesky black magic
  _solver_ptr->compute(_update);

  //ia zero the things
  _hessian_block_manager->setZero();
  _rhs_block_manager->setZero();
}


void Optimizer3D::_applyUpdate() {
  //ia take the things
  Graph3D* graph_3D = static_cast<Graph3D*>(_graph_ptr);
  IntVertexSE3PtrMap& vertices = graph_3D->vertices();

  //ia just in case
  if (_update_block_manager->dimension() != vertices.size()) {
    throw std::runtime_error("dimension mismatch in the graph update");
  }
  //ia apply the update to the graph
  //ia it is possible to cycle to the whole vertex container because 
  //ia the update blocks relative to a fixed vertex will be zero :D
  typename IntVertexSE3PtrMap::iterator v_it = vertices.begin();
  typename IntVertexSE3PtrMap::iterator v_end = vertices.end();
  while (v_it != v_end) {
    VertexSE3* v = v_it->second;
    const uint64_t& index = v->index();
    const Vector6& update_block = _update_block_manager->at(index);

    // std::cerr << "v[" << index << "]\n" << v->estimate().matrix() << std::endl;
    // std::cerr << "update_block = " << update_block.transpose() << std::endl;
    // std::cin.get();
    v->oplus(-update_block);

    ++v_it;
  }
}

} //ia namespace srrg_g2o_slim
