#pragma once

#include "types/base_optimizer.h"

namespace srrg_g2o_slim {

class Optimizer3D : public BaseOptimizer<Matrix6, Vector6> {
public:

  typedef MatrixBlockManager<Matrix6> MatrixBlockManager6;
  typedef VectorBlockManager<Vector6> VectorBlockManager6;
  typedef SparseBlockMatrix<Matrix6> SparseBlockMatrix6;
  typedef DenseBlockVector<Vector6> DenseBlockVector6;

  Optimizer3D();
  virtual ~Optimizer3D();

  virtual void clear();

  virtual void init();

  virtual void optimize();

protected:
  virtual void _oneStep();

  virtual void _applyUpdate();

  inline void _kernelize(const Real& chi_,
                         Matrix12& omega_,
                         Vector12& error_,
                         const bool suppress_outliers_ = false) {
    Real lambda = 1.0;

    if(chi_ > _kernel_width) {
      ++_outliers;
      if (suppress_outliers_)
        return;
      lambda = sqrtf(_kernel_width / chi_);
    } else {
      ++_inliers;
    }

    omega_ *= lambda;
    error_ *= lambda;
  }

  //! @brief manages the storage of the Hessian matrix
  MatrixBlockManager6* _hessian_block_manager = nullptr;

  //! @brief manages the storage of the rhs vector
  VectorBlockManager6* _rhs_block_manager = nullptr;

  //! @brief Hessian view;
  SparseBlockMatrix6* _hessian = nullptr;

  //! @brief RHS view
  DenseBlockVector6* _rhs = nullptr;

  //! @brief manages the storage of the update vector
  VectorBlockManager6* _update_block_manager = nullptr;

  //! @brief update vector
  DenseBlockVector6* _update = nullptr;

  //! @brief false if not initialized
  bool _initialized;
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

} //ia namespace srrg_g2o_slim

