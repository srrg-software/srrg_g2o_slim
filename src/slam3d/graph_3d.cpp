#include "graph_3d.h"
#include "utilities/unscented.h"

namespace srrg_g2o_slim {

Matrix12 _reconditionateOmega(const Matrix12& src_sigma_) {
  Matrix12 conditioned_omega = Matrix12::Zero();
  Matrix3 conditioned_omega_trans = Matrix3::Zero();
  Matrix9 conditioned_omega_rot = Matrix9::Zero();
  Matrix3 sigma_trans = src_sigma_.block<3,3>(9,9);
  Matrix9 sigma_rot = src_sigma_.block<9,9>(0,0);
  Vector3 recondioned_sv_trans = Vector3::Zero();
  Vector3 inverse_recondioned_sv_trans = Vector3::Zero();
  Vector9 recondioned_sv_rot = Vector9::Zero();
  Vector9 inverse_recondioned_sv_rot = Vector9::Zero();
  Eigen::JacobiSVD<Matrix3> svd_trans(sigma_trans, Eigen::ComputeThinU | Eigen::ComputeThinV);
  Eigen::JacobiSVD<Matrix9> svd_rot(sigma_rot, Eigen::ComputeThinU | Eigen::ComputeThinV);
  Eigen::JacobiSVD<Matrix12> svd(src_sigma_, Eigen::ComputeThinU | Eigen::ComputeThinV);
  const Matrix12& V = svd.matrixV();
  const Matrix12& U = svd.matrixU();
  // std::cerr << GREEN << svd.singularValues() << std::endl << RESET << std::endl;

  double max_ratio = svd_trans.singularValues()(0,0) / svd_rot.singularValues()(8,8);

  for (int i = 0; i < 3; ++i) {
    double s = svd_trans.singularValues()(i,i);
    if (max_ratio > 1e6) {
      s *= 1e-4;
    }
    recondioned_sv_trans[i] = s;
    inverse_recondioned_sv_trans[i] = 1./s;
    conditioned_omega_trans.noalias() += inverse_recondioned_sv_trans[i] * 
                                        svd_trans.matrixV().col(i) * 
                                        svd_trans.matrixU().col(i).transpose();
  }

  for (int i = 0; i < 9; ++i) {
    double s = svd_rot.singularValues()(i,i);
    if (s < 1e-7)
      s = 1e-7;
    recondioned_sv_rot[i] = s;
    inverse_recondioned_sv_rot[i] = 1./s;
    conditioned_omega_rot.noalias() += inverse_recondioned_sv_rot[i] * 
                                        svd_rot.matrixV().col(i) * 
                                        svd_rot.matrixU().col(i).transpose();
  }

  conditioned_omega.setZero();
  for (int i = 0; i < 12; ++i) {
    conditioned_omega.noalias() += (1./svd.singularValues()(i,i)) * V.col(i) * U.col(i).transpose();
  }
  // std::cerr << "normal omega" << std::endl;
  // std::cerr << conditioned_omega << std::endl;

  conditioned_omega.setZero();
  conditioned_omega.block<9,9>(0,0) = conditioned_omega_rot;
  conditioned_omega.block<3,3>(9,9) = conditioned_omega_trans;
  // std::cerr << "my omega" << std::endl;
  // std::cerr << conditioned_omega << std::endl;

  // std::cin.get();
  return conditioned_omega;
}

Matrix12 _reconditionateSigma(const Matrix12& src_sigma_,
                                const uint8_t type_,
                                const double& threshold_) {

  Matrix12 conditioned_sigma = Matrix12::Zero();

  switch (type_) {
    //ia soft conditioning
    case 0:
    {
      Eigen::JacobiSVD<Matrix12> svd(src_sigma_, Eigen::ComputeThinU | Eigen::ComputeThinV);
      double conditioned_eigenvalue = 1.0;
      double scale = 1;
      int exp = -1;
      double s = 0.0;
      for (int i = 0; i < 12; ++i) {
        // std::cerr << GREEN << "singular_v = " << svd.singularValues()(i,i) << std::endl << RESET;
        //! best
        if (svd.singularValues()(i,i) < threshold_) {
          conditioned_eigenvalue = svd.singularValues()(i,i) + threshold_;
        } else {
          conditioned_eigenvalue = svd.singularValues()(i,i);
        }
        conditioned_sigma.noalias() += conditioned_eigenvalue *
                                       svd.matrixU().col(i) * 
                                       svd.matrixU().col(i).transpose();
        // std::cerr << "conditioned_eigenvalue = " << conditioned_eigenvalue << std::endl;
      }
      // std::cin.get();
      break;
    }
    //ia mid conditioning - pump the value on the diagonal
    case 1:
    {
      conditioned_sigma = src_sigma_;
      conditioned_sigma.diagonal().array() += threshold_; //! Avoids rank loss (right value)
      // conditioned_sigma.diagonal().array() += 1e-10; //! Avoids rank loss (this value will restore original omegas +/-)
    }
    //ia hard conditioning - remove the shit outside of the diagonal (pig-style)
    case 2:
    {
      Matrix12 zero_diag_matrix = src_sigma_;
      zero_diag_matrix.diagonal().array() -= src_sigma_.diagonal().array();
      conditioned_sigma = src_sigma_ - zero_diag_matrix;
      break;
    }
    default:
      std::cerr << "wrong conditionig type; allowed are -> 0 [soft], 1 [mid], 2 [hard]" << std::endl;
      break;
  }
  return conditioned_sigma;
}


Matrix12 _remapInformationMatrix(const Vector6& src_mean_,
                                 const Matrix6& src_omega_,
                                 const uint8_t reconditioning_type_,
                                 const double& threshold_) {
  //ia computing sigma_12D
  Matrix6 src_sigma = src_omega_.inverse();
  Isometry3 T_mean = v2tMQT(src_mean_);

  Vector12 remapped_mean = Vector12::Zero();
  Matrix12 remapped_sigma = Matrix12::Zero();

  SigmaPoint6Vector sigma_points_6D;
  SigmaPoint12Vector sigma_points_12D;

  Vector6 zero_mean = Vector6::Zero();
  sampleUnscented(zero_mean, src_sigma, sigma_points_6D);

  int k = 1;
  for (int i = 0; i < src_mean_.size(); ++i) {
    int sample_plus_idx = k++;
    int sample_minus_idx = k++;

    const Vector6& sample_6D_plus = sigma_points_6D[sample_plus_idx].sample;
    const Vector6& sample_6D_minus = sigma_points_6D[sample_minus_idx].sample;

    Isometry3 T_plus = v2tMQT(sample_6D_plus);
    Isometry3 T_minus = v2tMQT(sample_6D_minus);
    // Isometry3 T_plus = v2t(sample_6D_plus);
    // Isometry3 T_minus = v2t(sample_6D_minus);

    Vector12 sample_12D_plus = flattenIsometry(T_mean*T_plus);
    Vector12 sample_12D_minus = flattenIsometry(T_mean*T_minus);

    SigmaPoint12 point_12D_plus(sample_12D_plus, sigma_points_6D[sample_plus_idx].wi, sigma_points_6D[sample_plus_idx].wp);
    SigmaPoint12 point_12D_minus(sample_12D_minus, sigma_points_6D[sample_minus_idx].wi, sigma_points_6D[sample_minus_idx].wp);
    sigma_points_12D.push_back(point_12D_plus);
    sigma_points_12D.push_back(point_12D_minus);
  }

  reconstructGaussian(sigma_points_12D, remapped_mean, remapped_sigma);
  
  //ia reconditioning the new covariance
  Matrix12 conditioned_sigma = _reconditionateSigma(remapped_sigma,reconditioning_type_, threshold_);
  Matrix12 remapped_omega = conditioned_sigma.inverse();
  // remapped_omega.block<9,9>(0,0) = Matrix9::Identity();
  // remapped_omega.block<3,3>(9,9) = Matrix3::Identity();
  return remapped_omega;
}

//ia from 12D back to standard 6D
Matrix6 _remapBackInformationMatrix(const Vector12& src_mean_,
                                    const Matrix12& src_omega_) {

  const Matrix12 src_sigma = src_omega_.inverse();

  Vector6 remapped_mean = Vector6::Zero();
  Matrix6 remapped_sigma = Matrix6::Zero();
  Matrix6 remapped_omega = Matrix6::Zero();
  
  SigmaPoint12Vector sigma_points_12D;
  SigmaPoint6Vector sigma_points_6D;

  sampleUnscented(src_mean_, src_sigma, sigma_points_12D);

  int k = 1;
  for (int i = 0; i < src_mean_.size(); ++i) {
    int sample_plus_idx = k++;
    int sample_minus_idx = k++;

    const SigmaPoint12& sigma_point12_plus = sigma_points_12D[sample_plus_idx];
    const SigmaPoint12& sigma_point12_minus = sigma_points_12D[sample_minus_idx];

    const Vector12& sample_12D_plus = sigma_point12_plus.sample;
    const Vector12& sample_12D_minus = sigma_point12_minus.sample;

    Isometry3 T_plus = unflattenIsometry(sample_12D_plus, true);
    Isometry3 T_minus = unflattenIsometry(sample_12D_minus, true);

    Vector6 sample_6D_plus = t2vMQT(T_plus);
    Vector6 sample_6D_minus = t2vMQT(T_minus);
    // Vector6 sample_6D_plus = t2v(T_plus);
    // Vector6 sample_6D_minus = t2v(T_minus);

    SigmaPoint6 point_6D_plus(sample_6D_plus, 
      sigma_point12_plus.wi, 
      sigma_point12_plus.wp);
    SigmaPoint6 point_6D_minus(sample_6D_minus, 
      sigma_point12_minus.wi, 
      sigma_point12_minus.wp);
    sigma_points_6D.push_back(point_6D_plus);
    sigma_points_6D.push_back(point_6D_minus);
  }

  reconstructGaussian(sigma_points_6D, remapped_mean, remapped_sigma);
  remapped_omega = remapped_sigma.inverse();
  return remapped_omega;
}


Graph3D::Graph3D() : BaseGraph() {
  _omega_threshold = 1e-1;
}


Graph3D::~Graph3D() {
  //ia delete vertexes
  if (_vertices.size()) {
    IntVertexSE3PtrMap::iterator v_it = _vertices.begin();
    IntVertexSE3PtrMap::iterator v_end = _vertices.end();

    while (v_it != v_end) {
      delete v_it->second;
      ++v_it;
    }
    _vertices.clear();
  } 
  
  //ia delete edges
  if (_edges.size()) {
    for (uint32_t e_idx = 0; e_idx < _edges.size(); ++e_idx) {
      delete _edges[e_idx];
    }
    _edges.clear();
  }

  //ia deleting sensors offsets
  _sensors_offsets.clear();

}


void Graph3D::clear() {
  //ia delete vertexes
  if (_vertices.size()) {
    IntVertexSE3PtrMap::iterator v_it = _vertices.begin();
    IntVertexSE3PtrMap::iterator v_end = _vertices.end();

    while (v_it != v_end) {
      delete v_it->second;
      ++v_it;
    }
    _vertices.clear();
  } else {
    std::cerr << "no vertices to delete" << std::endl;
  }
  
  //ia delete edges
  if (_edges.size()) {
    for (uint32_t e_idx = 0; e_idx < _edges.size(); ++e_idx) {
      delete _edges[e_idx];
    }
    _edges.clear();
  } else {
    std::cerr << "no edges to delete" << std::endl;
  }

  //ia deleting sensors offsets
  _sensors_offsets.clear();

  //ia zeroing the counters
  _num_edges = 0;
  _num_vertices = 0;
  _num_fixed_vertices = 0;
  _vertex_index = 0;
}


void Graph3D::load(const std::string& filename_) {
  std::cerr << std::endl;
  std::cerr << "loading graph from: " << YELLOW << filename_ << RESET << std::endl;
  std::fstream file(filename_);
  std::string line;

  //ia declare all the stuff needed
  int fixed_vertex_id = 0;
  int sensor_id = -1;
  Vector3 t;
  QuaternionReal q;

  //ia start parsing the g2o file
  while (getline(file, line)) {
    std::stringstream ss(line);
    std::string element_type;
    ss >> element_type;

    //TODO EDGE_SE3:QUAT -> EDGE_SE3:CHORD
    if (element_type == "#") {
      continue;

    } else if (element_type == "FIX") {
      ss >> fixed_vertex_id;
      _vertices.at(fixed_vertex_id)->setFixed(true);
      ++_num_fixed_vertices;

    } else if (element_type == "VERTEX_SE3:QUAT" || 
               element_type == "VERTEX_SE3:CHORD") { 
      int id = -1;
      ss >> id;
      ss >> t.x() >> t.y() >> t.z();
      ss >> q.x() >> q.y() >> q.z() >> q.w();

      Isometry3 T = Isometry3::Identity();
      T.linear() = q.matrix();
      T.translation() = t;

      addVertexSE3(id, T);

    } else if (element_type == "EDGE_SE3:QUAT") {
      int from_v_id = -1, to_v_id = -1;
      ss >> from_v_id >> to_v_id;

      ss >> t.x() >> t.y() >> t.z();
      ss >> q.x() >> q.y() >> q.z() >> q.w();

      Isometry3 T = Isometry3::Identity();
      T.linear() = q.matrix();
      T.translation() = t;

      Matrix6 src_omega = Matrix6::Identity();
      for (int i = 0; i < src_omega.rows(); ++i) {
        for (int j = i; j < src_omega.cols(); ++j) {
          ss >> src_omega(i,j);
          if (i != j)
            src_omega(j,i) = src_omega(i,j);
        }
      }

      addEdgeSE3(-1,from_v_id, to_v_id, T, src_omega);

    } else if (element_type == "EDGE_SE3:CHORD") {
      int from_v_id = -1, to_v_id = -1;
      ss >> from_v_id >> to_v_id;

      ss >> t.x() >> t.y() >> t.z();
      ss >> q.x() >> q.y() >> q.z() >> q.w();

      Isometry3 T = Isometry3::Identity();
      T.linear() = q.matrix();
      T.translation() = t;

      //OMEGAS MUST BE TRANSFORMED WITH UNSCENTED
      Matrix12 src_omega = Matrix12::Identity();
      for (int i = 0; i < src_omega.rows(); ++i) {
        for (int j = i; j < src_omega.cols(); ++j) {
          ss >> src_omega(i,j);
          if (i != j)
            src_omega(j,i) = src_omega(i,j);
        }
      }

      addEdgeSE3(-1, from_v_id, to_v_id, T, src_omega);
    } else if (element_type == "PARAMS_SE3OFFSET") {
      Isometry3 T = Isometry3::Identity();

      ss >> sensor_id;
      ss >> t.x() >> t.y() >> t.z();
      ss >> q.x() >> q.y() >> q.z() >> q.w();

      T.linear() = q.matrix();
      T.translation() = t;

      Isometry3 invT = T.inverse();
      _sensors_offsets.insert(std::make_pair(sensor_id, invT));
    } else {
      continue;
    }
  }

  file.close();

  if (_num_fixed_vertices == 0) {
    _vertices.begin()->second->setFixed(true);
  }

  //ia some log
  std::cerr << GREEN << "File loaded successfully" << RESET << std::endl;
  std::cerr << "vertices = " << _num_vertices << std::endl;
  std::cerr << "edges    = " << _num_edges << std::endl;
  std::cerr << "fixed    = " << _num_fixed_vertices << std::endl;
  std::cerr << std::endl;
}


void Graph3D::save(const std::string& filename_) const {
  std::cerr << "saving graph in " << GREEN << filename_ << RESET << std::endl;
  std::ofstream file(filename_);

  //ia full resolution in writing things
  // file << std::setprecision(std::numeric_limits<double>::digits10);

  Vector3 t;
  QuaternionReal q;

  //ia first: params offsets
  IntIsometry3Map::const_iterator s_it = _sensors_offsets.begin();
  IntIsometry3Map::const_iterator s_end = _sensors_offsets.end();
  while (s_it != s_end) {
    const Isometry3 invT = s_it->second.inverse();
    q = QuaternionReal(invT.linear());
    t = invT.translation();

    file << "PARAMS_SE3OFFSET" << " ";
    file << s_it->first << " ";

    file << t.x() << " " << t.y() << " " << t.z() << " ";
    file << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << " ";
    file << std::endl;
    ++s_it;
  }

  file << std::endl;

  //ia now vertices
  IntVertexSE3PtrMap::const_iterator v_it = _vertices.begin();
  IntVertexSE3PtrMap::const_iterator v_end = _vertices.end();
  while (v_it != v_end) {
    const VertexSE3* v = v_it->second;

    if (v->dimension() != BaseVertex<Isometry3>::VertexMinimalDim::VERTEX_SE3) {
      throw std::runtime_error("unknown vertex type");
    }

    const Isometry3& T = v->estimate();
    t = T.translation();
    q = QuaternionReal(T.linear());

    file << "VERTEX_SE3:CHORD" << " ";
    file << v->id() << " ";
    file << t.x() << " " << t.y() << " " << t.z() << " ";
    file << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << " " << std::endl;

    if (v->fixed()) {
      file << "FIX" << " " << v->id() << std::endl;
    }

    ++v_it;
  }

  file << "\n\n\n";

  //ia finally edges
  for (uint64_t e_idx = 0; e_idx < _edges.size(); ++e_idx) {
    const EdgeSE3* e = _edges[e_idx];

    file << "EDGE_SE3:CHORD" << " ";
    const int v_from_id = e->vertexAssociation().from_ptr->id();
    const int v_to_id = e->vertexAssociation().to_ptr->id();
    const Isometry3& T = e->measurement();
    const Matrix12& omega = e->information();

    t = T.translation();
    q = QuaternionReal(T.linear());

    file << v_from_id << " " << v_to_id << " ";
    file << t.x() << " " << t.y() << " " << t.z() << " ";

    file << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << " ";

    for (int r = 0; r < omega.rows(); ++r) {
      for (int c = r; c < omega.cols(); ++c) {
        file << omega(r,c) << " ";
      }
    }

    file << std::endl;
  }

  file << "\n\n";
  file.close();
}

void Graph3D::addVertexSE3(const uint64_t& id_, 
                           const Isometry3& estimate_) {
  VertexSE3* v = new VertexSE3(id_, _vertex_index, estimate_);
  _vertices.insert(std::make_pair(id_, v));
  ++_vertex_index;
  ++_num_vertices;
}

void Graph3D::addEdgeSE3(const int sensor_id_,
                         const uint64_t& v_from_id_,
                         const uint64_t& v_to_id_,
                         const Isometry3& measurement_,
                         const Matrix12& omega_) {
  EdgeSE3* e = new EdgeSE3(_vertices.at(v_from_id_),
                           _vertices.at(v_to_id_),
                           measurement_,
                           omega_,
                           sensor_id_);
  _edges.push_back(e);
  ++_num_edges;
}


void Graph3D::addEdgeSE3(const int sensor_id_,
  const uint64_t& v_from_id_,
  const uint64_t& v_to_id_,
  const Isometry3& measurement_,
  const Matrix6& omega_) {

  //ia transform the omega - with mild conditioning
  Vector6 mean = t2vMQT(measurement_);
  // Vector6 mean = t2v(measurement_);
  Matrix12 remapped_omega = _remapInformationMatrix(mean,omega_,0,_omega_threshold); 

  EdgeSE3* e = new EdgeSE3(_vertices.at(v_from_id_),
                           _vertices.at(v_to_id_),
                           measurement_,
                           remapped_omega,
                           sensor_id_);
  _edges.push_back(e);
  ++_num_edges;
}

} //ia namespace srrg_g2o_slim */
