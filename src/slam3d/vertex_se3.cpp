#include "utilities/geometry_utils.h"
#include "vertex_se3.h"

namespace srrg_g2o_slim {

VertexSE3::VertexSE3(const uint64_t& id_,
                     const uint64_t& vertex_index_,
                     const Isometry3& estimate_,
                     bool fixed_) :
                         BaseVertex<Isometry3>(id_,
                                               vertex_index_,
                                               VERTEX_SE3,
                                               estimate_,
                                               fixed_) {
}

VertexSE3::~VertexSE3() {}

void VertexSE3::oplus(const Vector6& update_) {
  Isometry3 new_estimate = v2t(update_) * _estimate;

  const Matrix3 rotation = new_estimate.linear();
  Matrix3 rotation_squared = rotation.transpose() * rotation;
  rotation_squared.diagonal().array() -= 1;
  new_estimate.linear() -= 0.5*rotation*rotation_squared;

  _estimate = new_estimate;
}

} //ia namespace srrg_g2o_slim
