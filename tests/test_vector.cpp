#include "types/dense_block_vector.h"
#include "utilities/system_utils.h"
#include <algorithm>

using namespace srrg_g2o_slim;

int main(int argc, char const *argv[]) {
  std::cerr << "Test vector" << std::endl;

  //ia instanciate a block manager of the desired type
  VectorBlockManager<Vector3>* bm0 = new VectorBlockManager<Vector3>();

  //ia allocate blocks and set some values inside
  for (uint32_t i = 0; i < 5; ++i) {
    Vector3* b = bm0->allocateOneBlock(i);
    *b = Vector3::Ones() * i;
  }

  //ia create a vector 'view' on top of the block manager - memory is shared
  DenseBlockVector<Vector3>* v0 = new DenseBlockVector<Vector3>(*bm0);
  std::cerr << "v0:" << std::endl;
  std::cerr << "size = " << GREEN << v0->size() << std::endl << RESET;
  v0->setRandom();
  v0->print();

  //ia create a new vector from v0 - memory is shared again
  DenseBlockVector<Vector3>* v1 = new DenseBlockVector<Vector3>(*v0);
  std::cerr << std::endl << "v1:" << std::endl;
  std::cerr << "size = " << GREEN << v1->size() << std::endl << RESET;
  v1->print();

  //ia create a new vector from v0 - deep copy
  DenseBlockVector<Vector3>* v2 = new DenseBlockVector<Vector3>(*v0, true);
  std::cerr << std::endl << "v2:" << std::endl;
  std::cerr << "size = " << GREEN << v2->size() << std::endl << RESET;
  v2->print();

  //ia sets to 0 v0 and v1 but not v2
  std::cerr << "bm0->setZero" << std::endl;
  bm0->setZero();
  std::cerr << std::endl << "v0:" << std::endl;
  std::cerr << "size = " << GREEN << v0->size() << std::endl << RESET;
  v0->print();
  std::cerr << std::endl << "v1:" << std::endl;
  std::cerr << "size = " << GREEN << v1->size() << std::endl << RESET;
  v1->print();
  std::cerr << std::endl << "v2:" << std::endl;
  std::cerr << "size = " << GREEN << v2->size() << std::endl << RESET;
  v2->print();

  //ia create a new vector with 10 blocks
  DenseBlockVector<Vector2>* v3 = new DenseBlockVector<Vector2>(10);
  //ia assign values to the blocks
  for (uint32_t i = 0; i < 10; ++i) {
    v3->at(i) = Vector2::Ones() * i;
  }
  std::cerr << std::endl << "v3:" << std::endl;
  std::cerr << "size = " << GREEN << v3->size() << std::endl << RESET;
  v3->print();


  //ia create a new vector initially with no blocks; vector owns the memory
  DenseBlockVector<Vector6>* v4 = new DenseBlockVector<Vector6>();
  //ia create 3 blocks
  for (uint32_t i = 0; i < 3; ++i) {
    if(!v4->createBlock(i)) {
      std::cerr << "something went wrong :/" << std::endl;
    }
  }
  std::cerr << std::endl << "v4:" << std::endl;
  std::cerr << "size = " << GREEN << v4->size() << std::endl << RESET;
  v4->print(); //ia all zero blocks

  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "test permutations:O\n" << std::endl;

  DenseBlockVector<Vector2>* v5 = new DenseBlockVector<Vector2>(10);
  std::cerr << "size = " << GREEN << v5->size() << std::endl << RESET;
  IntVector p(v5->size());
  int p_entry = v5->size()-1;
  for (uint32_t i = 0; i < v5->size(); ++i) {
    v5->at(i) = Vector2::Ones()*i;
    p[i] = p_entry;
    --p_entry;
  }

  std::cerr << "random permutation" << std::endl;
  std::random_shuffle(p.begin(), p.end());
  for (int k = 0; k < p.size(); ++k) {
    std::cerr << p[k] << " ";
  }
  std::cerr << std::endl;
  v5->print();

  std::cerr << "apply a perm" << std::endl;
  v5->applyPermutation(p);
  std::cerr << "size = " << GREEN << v5->size() << std::endl << RESET;
  v5->print();

  std::cerr << "apply inverse perm" << std::endl;
  v5->applyInversePermutation(p);
  std::cerr << "size = " << GREEN << v5->size() << std::endl << RESET;
  v5->print();




  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "multiple deletions :/\n" << std::endl;

  DenseBlockVector<Vector6>* v6 = new DenseBlockVector<Vector6>(50000);
  int porcodio = 1;
  for (int i = 0; i < v6->size(); ++i) {
      v6->at(i) = Vector6::Ones() * porcodio++;
  }
  std::cerr << "size = " << GREEN << v6->size() << std::endl << RESET;
  // v6->print();

  std::set<int> removed_indexes;
  std::vector<int> real_removed_indexes;
  removed_indexes.insert(1);
  removed_indexes.insert(4);
  

  std::set<int>::const_iterator it = removed_indexes.begin();
  for ( ; it != removed_indexes.end(); ++it) {
    std::cerr << GREEN << *it << std::endl << RESET;
  }

  int decrementer = 0;
  it = removed_indexes.begin();
  for ( ; it != removed_indexes.end(); ++it) {
    int new_index = *it-decrementer++;
    if(new_index < 0)
      throw std::runtime_error("negative index");
    real_removed_indexes.push_back(new_index);
  }

  double t0 = 0, t1 = 0;
  for (int j = 0; j < real_removed_indexes.size(); ++j) {
    std::cerr << real_removed_indexes[j] << std::endl;
    t0 = getTime();
    v6->removeBlock(real_removed_indexes[j]);
    t1 = getTime();
    std::cerr << "TIME = " << t1-t0 << std::endl;
  }

  std::cerr << "size = " << GREEN << v6->size() << std::endl << RESET;


  delete bm0;
  delete v0;
  delete v1;
  delete v2;
  delete v3;
  delete v4;
  delete v5;
  delete v6;
  return 0;
}
