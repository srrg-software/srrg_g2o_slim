#include "slam3d/graph_3d.h"

using namespace srrg_g2o_slim;

int main(int argc, char *argv[]) {
  if (argc < 3 || argc > 3) {
    std::cerr << "usage: " << argv[0] << " <input.g2o> <output.g2o>" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string in_filename(argv[1]);
  std::string out_filename(argv[2]);

  Graph3D* g = new Graph3D();
  g->load(in_filename);
  std::cerr << "v   : " << g->numVertices() << std::endl;
  std::cerr << "e   : " << g->numEdges() << std::endl;
  std::cerr << "fix : " << g->numFixed() << std::endl;

  std::cerr << "v[0]" << std::endl;
  std::cerr << g->vertices().begin()->second->estimate().matrix() << std::endl;
  std::cerr << "e[0]" << std::endl;
  std::cerr << g->edges().at(0)->measurement().matrix() << std::endl;
  std::cerr << std::endl;
  std::cerr << g->edges().at(0)->information() << std::endl;

  std::cerr << "save" << std::endl;
  g->save(out_filename);

  g->clear();
  std::cerr << "v   : " << g->numVertices() << std::endl;
  std::cerr << "e   : " << g->numEdges() << std::endl;
  std::cerr << "fix : " << g->numFixed() << std::endl;


  delete g;
  return 0;
}